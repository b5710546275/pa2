package ku.util;

import java.util.Arrays;
import java.util.EmptyStackException;

/**
 * A Stack that remove unnecessary method.
 * It is a array that you can only get the first element of the array and can only insert to the first slot of thte array.
 * @author Norawit Urailertprasert
 *
 * @param <T>
 *            the type of the element of the stack.
 */
public class Stack<T> {
	/**
	 * the array that is used to store the element.
	 */
	private T[] items;
	/**
	 * A constructor that used to create the array for the stack.
	 * @param capacity the size of the array/stack.
	 */
	public Stack(int capacity) {
		if(capacity<0)
			capacity=0;
		items = (T[]) new Object[capacity];
	}
	/**
	 * telling the capacity that the stack can hold.
	 * @return the number of element that this stack can hold.
	 */
	public int capacity() {
		if (items != null) {
			return items.length;
		}
		return -1;
	}
	/**
	 * Telling that the stack is empty or not.
	 * @return true if the stack is empty
	 */
	public boolean isEmpty() {
		for (T i : items) {
			if (i != null)
				return false;
		}
		return true;
	}
	/**
	 * Telling that the stack is full or not.
	 * By full mean all the slot of the array is not null.
	 * @return true if it full.
	 */
	public boolean isFull() {
		for (T i : items) {
			if (i == null)
				return false;
		}
		return true;
	}
	/**
	 * Peek is used when you want to look at the first element of the array but don't want to get it out of the array.
	 * @return first element of the array.
	 */
	public T peek() {
		if (isEmpty())
			return null;
		return items[0];
	}
	/**
	 * Pop is used when you want to get the element out from a stack.
	 * When you call this method it will return the first element of the array and remove that element out of an array.
	 * @return the first element of the stack.
	 */
	public T pop() {
		if (isEmpty())
			throw new EmptyStackException();
		T temp = items[0];
		for (int i = 0; i < items.length - 1; i++) {
			items[i] = items[i + 1];
		}
		items[items.length - 1] = null;
		return temp;
	}
	/**
	 * Push is a method used to insert an object in to a stacj/array.
	 * it will insert to the first slot.
	 * if the object is null it will throw an exception.
	 * if the the stack/array is full it will do nothing.
	 * @param obj
	 */
	public void push(T obj) {
		if (!isFull()) {
			if (obj == null)
				throw new IllegalArgumentException();
			for (int i = items.length-1; i >0 ; i--) {
				items[i] = items[i-1];
			}
			
			items[0] = obj;
		}
	}
	/**
	 * Size method is used when you want to know how many item is in the stck/array.
	 * @return number of item in a stack/array.
	 */
	public int size() {
		int count =0;
		for (int i = 0; i < items.length; i++) {
			if (items[i] != null)
				count ++;
		}
		return count;

	}
}
