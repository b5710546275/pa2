package ku.util;
import java.util.Iterator;
import java.util.NoSuchElementException;

/**An ArrayIerator is a iterator that that store object inform of a array.
 * 
 * @author Norawit Urailertprasert
 *
 * @param <T> the input type of the iterator.
 */
public class ArrayIterator<T> implements Iterator<T> {
	/**
	 * An array the store the element of the Iterator.
	 */
	private T[] array;
	/**
	 * A cursor is used to store the current position in an array.
	 */
	private int cursor =0;
	/**
	 * A constructor used to initialize the array.
	 * @param array
	 */
	public ArrayIterator(T[] array) {
		this.array = array;
	}
	@Override
	/**
	 * A hashNext is a method used to check if the array still have a element that is not null.
	 * It also move the cursor if the next element is null. For example move a cursor to the last index if all element befor is null.
	 *@return true if it has next element false if it doesn't has next element.
	 */
	public boolean hasNext() {
			for(int i=cursor;i<array.length;i++){
				if(array[i]!=null){
					cursor = i;
					return true;
				}
			}
		
		return false;
	}
	
	@Override
	/**
	 * Next is used to return the element from an array at the position of the cursor and also if it the element is not null.
	 *@return the element that the cursor is pointed.
	 */
	public T next() {
		if(hasNext()){
			T temp = array[cursor];
			cursor++;
			return temp;
		}	
		throw new NoSuchElementException();
	}
	/**
	 * Call when wanted to remove the current element that next call.
	 */
	public void remove(){
		array[cursor-1]=null;
	}
	
}
